from django.db import models
from easy_thumbnails.files import get_thumbnailer
from djmoney.models.fields import MoneyField

from ckeditor_uploader.fields import RichTextUploadingField
from solo.models import SingletonModel
from phone_field import PhoneField
from datetime import datetime 

#Import para las imagenes en galeria, si queremos asociar un apartamento con más de una imagen
from image_cropping import ImageRatioField 
from easy_thumbnails.files import get_thumbnailer
from PIL import Image as Img
from io import BytesIO
from PIL import ExifTags
from django.core.files import File
from django.core.files.uploadedfile import InMemoryUploadedFile
import sys, os

class Config(models.Model):
    title = models.CharField(max_length=50, default="", blank=True, verbose_name='title')
    logo = models.ImageField(verbose_name=u"Website logo", null=True, blank=True)
    phone = models.CharField(max_length=23, default="", blank=True, verbose_name='telephone contact')
    email = models.EmailField(max_length=127, default="", blank=True, verbose_name='Email contact')
    cif = models.CharField(max_length=50, verbose_name=u'CIF', default="", blank=True)
    address = models.CharField(verbose_name=u'Address', blank=True, max_length=200)
    description = models.TextField(blank=True, null=True, verbose_name=u'Description')

    legal = RichTextUploadingField(verbose_name=u'legal warning', blank=True, null=True)

    def __str__(self):
        return u"Website Configuration"

    def get_logo(self):
        return get_thumbnailer(self.logo).get_thumbnail({'size': (366, 63), 'box': self.cropping, 'crop': True, 'detail': True }).url 

    class Meta:
        verbose_name = u"Website Configuration"
        verbose_name_plural = u"Website Configuration"

class DiscountAndExtraPrice(models.Model):
    discount = models.IntegerField(blank=False, null=True, verbose_name='Discount in %')
    clean_house_price = MoneyField(max_digits=14, decimal_places=2, null=True, default_currency='DKK', verbose_name='Clean price')
    
    def __str__(self):
        return u"Discounts and extra price"

    class Meta:
        verbose_name = u"Discounts and extra price"
        verbose_name_plural = u"Discounts and extra price"

class CustomerInfo(models.Model):
    name = models.CharField(max_length=50, default="", blank=False, verbose_name='Name')
    first_surname = models.CharField(max_length=50, default="", blank=False, verbose_name='First Name')
    second_surname = models.CharField(max_length=50, default="", blank=False, verbose_name='Second Name')
    phone = PhoneField(blank=False, help_text='Contact phone number')
    address = models.CharField(max_length=150, default="", blank=False, verbose_name='Address')
    email = models.EmailField(max_length=254, blank=False)
    date_rented = models.DateTimeField(default=datetime.now)

    def __str__(self):
        return self.name + " " + self.first_surname + " " + self.second_surname

    class Meta:
        verbose_name = u"Customer"
        verbose_name_plural = u"Customers"

#Class abstract la cual usaremos para los dos appartamentos
class ApartmentDateModel(models.Model):
    name = models.CharField(max_length=15, default="Apartment", verbose_name='Name')
    start_date = models.DateField(blank=True)
    end_date = models.DateField(blank=True)

    customer = models.ForeignKey(CustomerInfo, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        abstract = True

class ApartmentDateDown(ApartmentDateModel):
    def __init__(self, *args, **kwargs):
        if 'name' not in kwargs:
            kwargs['name'] = "Apartment 1"
        super(ApartmentDateDown, self).__init__(*args, **kwargs)

    class Meta:
        verbose_name = u"Apartment rental date down"
        verbose_name_plural = u"Apartment rental date down"

class ApartmentDateUp(ApartmentDateModel):
    def __init__(self, *args, **kwargs):
        if 'name' not in kwargs:
            kwargs['name'] = "Apartment 2"
        super(ApartmentDateUp, self).__init__(*args, **kwargs)

    class Meta:
        verbose_name = u"Apartment rental date up"
        verbose_name_plural = u"Apartment rental date up"

class ApartmentImageModel(models.Model):
    image_title = models.CharField(max_length=100, blank=False)
    description_english = models.TextField(verbose_name='Description english', blank=True)
    description_danish = models.TextField(verbose_name='Description danish', blank=True) 
    description_deutch = models.TextField(verbose_name='Description deuth', blank=True)

    def __str__(self):
        return self.image_title

    def get_img(self):      
        return self.image

    class Meta:         
        abstract = True

def path_and_rename(instance, filename):
    ext = filename.split('.')[1]
    date_now = str(datetime.now().strftime("%Y-%m-%d %H:%M:%S") )
    return os.path.join(instance.get_folder_image_to_save(), date_now + "_" + instance.image_title + "." + ext)
    
#Nota para mi, crear una clase en otro fichero abstract que luego esta de abajo ere de de esa para no duplicar código
class ApartmentUpImagen(ApartmentImageModel):
    image = models.ImageField(upload_to=path_and_rename, max_length=500, blank=True, verbose_name='image')
    
    class Meta:         
    #    abstract = True         
        verbose_name = 'Image'         
        verbose_name_plural = u'Images Apartment Up'

    @staticmethod
    def get_folder_image_to_save():
        return 'ApartmentUpImage'

    def save(self, *args, **kwargs):
        if self.image:
            pilImage = Img.open(BytesIO(self.image.read()))
            for orientation in ExifTags.TAGS.keys():
                if ExifTags.TAGS[orientation] == 'Orientation':
                    break

            if pilImage._getexif() != None:
                exif = dict(pilImage._getexif().items())

                if exif[orientation] == 3:
                    pilImage = pilImage.rotate(180, expand=True)
                elif exif[orientation] == 6:
                    pilImage = pilImage.rotate(270, expand=True)
                elif exif[orientation] == 8:
                    pilImage = pilImage.rotate(90, expand=True)

            output = BytesIO()
            pilImage.save(output, format='JPEG', quality=100)
            output.seek(0)

            #change the imagefield value to be the newley modifed image value
            self.image = InMemoryUploadedFile(output,'ImageField', "%s.jpg" %self.image.name.split('.')[0], 'image/jpeg', sys.getsizeof(output), None)

        return super(ApartmentUpImagen, self).save(*args, **kwargs)       

class ApartmentDownImagen(ApartmentImageModel):
    image = models.ImageField(upload_to=path_and_rename, max_length=500, blank=True, verbose_name='image')
    
    class Meta:         
    #    abstract = True         
        verbose_name = 'Image'         
        verbose_name_plural = u'Images Apartment Down'

    @staticmethod
    def get_folder_image_to_save():
        return 'ApartmentDownImage'

    def save(self, *args, **kwargs):
        if self.image:
            pilImage = Img.open(BytesIO(self.image.read()))
            for orientation in ExifTags.TAGS.keys():
                if ExifTags.TAGS[orientation] == 'Orientation':
                    break

            if pilImage._getexif() != None:
                exif = dict(pilImage._getexif().items())

                if exif[orientation] == 3:
                    pilImage = pilImage.rotate(180, expand=True)
                elif exif[orientation] == 6:
                    pilImage = pilImage.rotate(270, expand=True)
                elif exif[orientation] == 8:
                    pilImage = pilImage.rotate(90, expand=True)

            output = BytesIO()
            pilImage.save(output, format='JPEG', quality=100)
            output.seek(0)

            #change the imagefield value to be the newley modifed image value
            self.image = InMemoryUploadedFile(output,'ImageField', "%s.jpg" %self.image.name.split('.')[0], 'image/jpeg', sys.getsizeof(output), None)

        return super(ApartmentDownImagen, self).save(*args, **kwargs)       

class PricesPerDay(models.Model):
    date = models.DateField(blank=True, unique=True)
    price_down = MoneyField(max_digits=14, decimal_places=2, default_currency='DKK', verbose_name='Price Apartment 1 (down)')
    price_up = MoneyField(max_digits=14, decimal_places=2, default_currency='DKK', verbose_name='Price Apartment 2 (up)')

class ContactUsQuestions(models.Model):
    date = models.DateTimeField(auto_now_add=True, blank=True)
    person_name = models.CharField(max_length=100, blank=False)
    email = models.EmailField(max_length=127, blank=False, verbose_name='Email contact')
    phone = models.IntegerField(blank=False, help_text='Contact phone number')
    prefix = models.IntegerField(blank=False, help_text='Prefix')
    message = models.TextField(max_length=4048, blank=False)
    close_ticket = models.BooleanField(blank=False, default=False, verbose_name='Close ticket')


