from django.shortcuts import render, redirect
from django.utils import translation
from django.utils.translation import ugettext as _
from .models import *
from .forms import *
from .utils import *

from django.conf import settings

from django.http import HttpResponse, JsonResponse, HttpResponseRedirect

import stripe # new

#import json

def index(request):
    form = ContactUsQuestionsForm()
    language = ""
    show_alert_error = False
    images_apartment_up = None

    if 'language' not in request.session:
        language = "en"
        translation.activate(get_language_code("bg"))
        images_apartment_down = get_image_and_description_apartment_down("bg")
        images_apartment_up = get_image_and_description_apartment_up("bg")
    else:
        language = request.session['language']
        images_apartment_down = get_image_and_description_apartment_down(request.session['language'])
        images_apartment_up = get_image_and_description_apartment_up(request.session['language'])

    #context['key'] = settings.STRIPE_PUBLISHABLE_KEY
    key = settings.STRIPE_PUBLISHABLE_KEY

    return render(request, 'CMS/index.html', locals())

def change_language(request, language_code):
    translation.activate(get_language_code(language_code))
    #request.session[translation.LANGUAGE_SESSION_KEY] = get_language_code(language_code)
    request.session['language'] = language_code
    language = language_code
    settings.LANGUAGE_CODE = get_language_code(language_code)

    return redirect('CMS:index')
    #return render(request, 'CMS/index.html', locals())

#Método que obtiene todos los dias que ha sido la casa alquilada
def get_apartment_rented_dates(request):
    dates_rented = {
        'dates_down': list(ApartmentDateDown.objects.values('start_date', 'end_date')),
        'dates_up': list(ApartmentDateUp.objects.values('start_date', 'end_date'))
    } 
    return JsonResponse(dates_rented)

#Método que obtiene los precios de todos los días de la parte de arriba y abajo de la casa
def get_apartment_prices_date(request):
    dates_prices = {
        'dates_prices': list(PricesPerDay.objects.values('date', 'price_down'.strip("0"), 'price_up'))
    } 
    return JsonResponse(dates_prices)

#Método para el calculo del precio de los dias seleccionados en el calendario
def get_price_calculated(request):
    context = {}
    days_to_calculate = []
    price = ""
    cleaning_switch = False

    if request.method == "GET" and request.is_ajax():
        days_to_calculate = request.GET.getlist('days_to_calculate[]')
        apartment_option = request.GET.get('apartment_option')
        cleaning_switch = request.GET.get('cleaning_switch')
        price = calculate_price(days_to_calculate, apartment_option, cleaning_switch)
        context['price'] = price
        
        return JsonResponse(context)

    return redirect('CMS:index')

#Método poara mandar un correo de preguntas del usuario
def send_message(request):
    if request.method == "POST":
        form = ContactUsQuestionsForm(request.POST)

        if form.is_valid():
            from_user = form.cleaned_data['email']
            message = form.cleaned_data['message']
            phone = form.cleaned_data['phone']
            prefix = form.cleaned_data['prefix']
            try:
                subject = "Kontakt: " + from_user + " eller telefon: " + "+" + str(prefix) + " " + str(phone)
                send_email(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_USER, subject, message, False)
                form.save()
                return HttpResponseRedirect('/')
            except Exception as e:
                return redirect('CMS:index')
                pass
        else:
            show_alert_error = True
            images_apartment_up = ApartmentUpImagen.objects.all()
            return render(request, 'CMS/index.html', locals())
    else:
        return redirect('CMS:index')

def charge(request):
    if request.method == 'POST':
        stripe.api_key = settings.STRIPE_SECRET_KEY
        charge = stripe.Charge.create(
            amount=500,
            currency='dkk',
            description='A Django charge',
            source=request.POST['stripeToken']
        )
        return render(request, 'CMS/charge.html')
