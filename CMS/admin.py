from django.contrib import admin
from .models import *

from image_cropping import ImageCroppingMixin
from solo.admin import SingletonModelAdmin
from jet.admin import CompactInline

class ConfigAdmin(ImageCroppingMixin, SingletonModelAdmin):
	
    fieldsets = (
            (u"General data", {
                'fields': ('title', 'logo', ('phone', 'email'), 'address', 'cif', 'description', )
            }),

        )

class DiscountAndExtraPriceAdmin(SingletonModelAdmin):
	
    fieldsets = (
            (u"Discount and extra price", {
                'fields': ('discount', 'clean_house_price',)
            }),

        )

class ApartmentUpImagenAdmin(admin.ModelAdmin):
    list_display = ('image_title', 'description_english', 'description_danish', 'description_deutch', 'image',)
    list_display_links = ('image_title',)

    fieldsets = (
            (u"Apartment image data", {
                'fields': ('image_title', 'description_english', 'description_danish', 'description_deutch', 'image',)
            }),
        )

class ApartmentDownImagenAdmin(admin.ModelAdmin):
    list_display = ('image_title', 'description_english', 'description_danish', 'description_deutch', 'image',)
    list_display_links = ('image_title',)

    fieldsets = (
            (u"Apartment image data", {
                'fields': ('image_title', 'description_english', 'description_danish', 'description_deutch', 'image',)
            }),
        )

class CustomerInfoAdmin(admin.ModelAdmin):
    list_display = ('name', 'first_surname', 'second_surname', 'phone', 'address', 'email', 'date_rented')
    list_display_links = ('name',)

    fieldsets = (
            (u"Customer info data", {
                'fields': ('name', 'first_surname', 'second_surname', 'phone', 'address', 'email',)
            }),
        )

class ApartmentDateDownAdmin(admin.ModelAdmin):
    list_display = ('start_date', 'end_date', 'customer', )

    fieldsets = (
            (u"Apartament info down", {
                'fields': ('start_date', 'end_date', 'customer', )
            }),
        )

class ApartmentDateUpAdmin(admin.ModelAdmin):
    list_display = ('start_date', 'end_date', 'customer', )

    fieldsets = (
            (u"Apartament info up", {
                'fields': ('start_date', 'end_date', 'customer', )
            }),
        )

class PricesPerDayAdmin(admin.ModelAdmin):
    list_display = ('date', 'price_down', 'price_up', )
    list_editable = ('price_down', 'price_up', )

    fieldsets = (
            (u"Apartament info up", {
                'fields': ('date', 'price_down','price_up', )
            }),
        )

class ContactUsQuestionsAdmin(admin.ModelAdmin):
    list_display = ('date', 'person_name', 'email', 'phone', 'prefix', 'message', 'close_ticket',)
    list_editable = ('close_ticket',)

admin.site.register(CustomerInfo, CustomerInfoAdmin)

admin.site.register(ApartmentDateDown, ApartmentDateDownAdmin)
admin.site.register(ApartmentDateUp, ApartmentDateUpAdmin)

admin.site.register(ApartmentUpImagen, ApartmentUpImagenAdmin)
admin.site.register(ApartmentDownImagen, ApartmentDownImagenAdmin)

admin.site.register(PricesPerDay, PricesPerDayAdmin)
admin.site.register(ContactUsQuestions, ContactUsQuestionsAdmin)

admin.site.register(Config, ConfigAdmin)
admin.site.register(DiscountAndExtraPrice, DiscountAndExtraPriceAdmin)
