from django import forms
from .models import ContactUsQuestions
from django.utils.translation import ugettext_lazy as _

class ContactUsQuestionsForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ContactUsQuestionsForm, self).__init__(*args, **kwargs)
        self.fields['person_name'].widget.attrs['placeholder'] = _("Name *")
        self.fields['email'].widget.attrs['placeholder'] = _("Email: example@gmail.com *")
        self.fields['phone'].widget.attrs['placeholder'] = _("Phone number *")
        self.fields['prefix'].widget.attrs['placeholder'] = _("34 *")
        self.fields['message'].widget.attrs['placeholder'] = _("Message *")

        self.fields['phone'].widget.attrs['id'] = "phone_id"
        self.fields['prefix'].widget.attrs['id'] = "prefix_id"

        self.fields['person_name'].widget.attrs['class'] = "form-control"
        self.fields['email'].widget.attrs['class'] = "form-control"
        #self.fields['phone'].widget.attrs['class'] = "form-control"
        self.fields['message'].widget.attrs['class'] = "form-control"

    class Meta:
        model = ContactUsQuestions
        fields = ['person_name', 'email', 'phone', 'message', 'prefix']



