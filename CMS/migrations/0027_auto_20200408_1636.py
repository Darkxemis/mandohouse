# Generated by Django 2.2.6 on 2020-04-08 14:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CMS', '0026_auto_20200408_1630'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contactusquestions',
            name='phone',
            field=models.IntegerField(help_text='Contact phone number'),
        ),
    ]
