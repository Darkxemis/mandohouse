# Generated by Django 2.2.6 on 2020-04-08 09:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CMS', '0019_auto_20200328_2131'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='appartementdownimagen',
            name='appartement_date_down',
        ),
        migrations.RemoveField(
            model_name='appartementupimagen',
            name='appartement_date_up',
        ),
        migrations.AlterField(
            model_name='appartementdownimagen',
            name='description_danish',
            field=models.TextField(blank=True, verbose_name='Description_danish'),
        ),
        migrations.AlterField(
            model_name='appartementdownimagen',
            name='description_deutch',
            field=models.TextField(blank=True, verbose_name='Description_deuth'),
        ),
        migrations.AlterField(
            model_name='appartementdownimagen',
            name='description_english',
            field=models.TextField(blank=True, verbose_name='Description_english'),
        ),
        migrations.AlterField(
            model_name='appartementupimagen',
            name='description_danish',
            field=models.TextField(blank=True, verbose_name='Description_danish'),
        ),
        migrations.AlterField(
            model_name='appartementupimagen',
            name='description_deutch',
            field=models.TextField(blank=True, verbose_name='Description_deuth'),
        ),
        migrations.AlterField(
            model_name='appartementupimagen',
            name='description_english',
            field=models.TextField(blank=True, verbose_name='Description_english'),
        ),
    ]
