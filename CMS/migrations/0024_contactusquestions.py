# Generated by Django 2.2.6 on 2020-04-08 14:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CMS', '0023_auto_20200408_1210'),
    ]

    operations = [
        migrations.CreateModel(
            name='ContactUsQuestions',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('person_name', models.CharField(max_length=100)),
                ('email', models.CharField(max_length=127, verbose_name='Email contact')),
                ('phone', models.DecimalField(decimal_places=0, help_text='Contact phone number', max_digits=8)),
                ('message', models.CharField(max_length=4048)),
            ],
        ),
    ]
