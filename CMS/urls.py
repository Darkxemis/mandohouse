from CMS import views
from django.urls import path

app_name='CMS'

urlpatterns = [
    path('', views.index, name="index"),
    path('change_language/<str:language_code>', views.change_language, name="change_language"),
    path('get_apartment_rented_dates', views.get_apartment_rented_dates, name="get_apartment_rented_dates"),
    path('get_apartment_prices_date', views.get_apartment_prices_date, name="get_apartment_prices_date"),
    path('get_price_calculated', views.get_price_calculated, name="get_price_calculated"),
    path('send_message', views.send_message, name="send_message"),
    path('charge/', views.charge, name='charge'), # new
    
]