(function($) {
  "use strict"; // Start of use strict

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top - 54)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#mainNav',
    offset: 56
  });

  $( "#StartDate" ).focus(function() {
    input = this;
    //$("#StartDate" ).css({'box-shadow' : '0 0 50px rgb(216, 181, 28)'});
    $("#StartDate" ).css('box-shadow', '0px 0px 50px #d8b51c');
    
    start_date_input_value = undefined;
    $( "#EndDate" ).val("");
    $("#EndDate" ).css({'background-color' : '#a19a9a'});
    $("#EndDate").prop('disabled', true);

    $('#price-container').hide();

    loadDaysRented();
    
    //$('#calendar_1').show();
  });

  $( "#EndDate" ).focus(function() {
    input = this;
    $("#EndDate" ).css('box-shadow', '0px 0px 50px #d8b51c');
    loadDaysRented();
    //$('#calendar_1').show();
  });

  $( "#StartDate" ).change(function() {
    //$( "#EndDate" ).show();
    $("#EndDate").prop('disabled', false);
    $( "#EndDate" ).focus();
    $("#EndDate" ).css({'background-color' : 'white'});
    start_date_input_value = new Date($( "#StartDate" ).val());
    //calendar.refresh();
  });

  $('#cleaning-switch').change(function() {
    let days_to_calculate = fillDatesBetween($( "#StartDate" ).val(), $( "#EndDate" ).val());
    let radio_button_selected = $('input:radio[name=radio]:checked').attr('id');
    let cleaning_switch = $('input[name=cleaning-switch]').is(':checked');

    calculatePrice(days_to_calculate, radio_button_selected, cleaning_switch);        
  });

  $( "#EndDate" ).change(function() {
    let days_to_calculate = fillDatesBetween($( "#StartDate" ).val(), $( "#EndDate" ).val());
    let radio_button_selected = $('input:radio[name=radio]:checked').attr('id');
    let cleaning_switch = $('input[name=cleaning-switch]').is(':checked');

    calculatePrice(days_to_calculate, radio_button_selected, cleaning_switch);
  });

  /*Calcula el precio total de los dias que se van alquilar y los añade en la vista*/
  function calculatePrice(days_to_calculate, apartment_option, cleaning_switch) {
    $.ajax({ 
      data: {'days_to_calculate': days_to_calculate, 'apartment_option': apartment_option, 'cleaning_switch': cleaning_switch}, 
      type: "GET", 
      url: "../get_price_calculated",
      success: function(json) {
        setNewPrice(json.price);
      },
      error : function(message) {
        console.log(message);
      }
    });
  };

  function setNewPrice(price) {
    let decimal_price = price.split('.')[1];

    if(decimal_price==='00')
    {
      price=price.split('.')[0];
    }

    /*Moneda por defecto*/ 
    $("#price-text").text(price + " dkk");
    $('#price-container').show();
    $('#book-now').show();
  };

  $('input[type=radio][name=radio]').click(function() {
    $("#StartDate").prop('disabled', false);
    $("#StartDate").focus();
    $("#StartDate" ).css({'background-color' : 'white'});
    
    $("#EndDate").prop('disabled', true);
    $("#EndDate" ).css({'background-color' : '#a19a9a'});

    $('#price-container').hide();
    $('#book-now').hide();
    
    $( "#StartDate" ).val("");
    $( "#EndDate" ).val("");

    loadDaysRented();
  });

  function loadDaysRented() {
    $.ajax({ 
      //data: {'id_note': id_note}, 
      type: "GET", 
      url: "../get_apartment_rented_dates",
      success: function(json) {
        json_dates_rented = json;
        loadArrayDaysRented();
      },
      error : function(message) {
        console.log(message);
      }
    });
  };

  function loadArrayDaysRented() {
    let radio_button_selected = $('input:radio[name=radio]:checked').attr('id');
    days_rented_apartment = [];

    if (json_dates_rented != undefined) {
      if (radio_button_selected === "apartment-down") {
        $.each(json_dates_rented.dates_down, function(i, obj) {
          $.merge(days_rented_apartment, fillDatesBetween(obj.start_date, obj.end_date));
        });
      } else if (radio_button_selected === "apartment-up") {
        $.each(json_dates_rented.dates_up, function(i, obj) {
          $.merge(days_rented_apartment, fillDatesBetween(obj.start_date, obj.end_date));
        });
      } else {
        $.each(json_dates_rented.dates_down, function(i, obj) {
          $.merge(days_rented_apartment, fillDatesBetween(obj.start_date, obj.end_date));
        });

        $.each(json_dates_rented.dates_up, function(i, obj) {
          $.merge(days_rented_apartment, fillDatesBetween(obj.start_date, obj.end_date));
        });
      }
      calendar.refresh();
    }
  };

  var days_rented_apartment = [];
  var date_prices = [], prices_down = [], prices_up = [];
  var calendar;
  var input;
  var json_dates_rented;
  var start_date_input_value;

  $(document).ready(function() {
    let go_to_contact_when_error =  $('#go_to_contact').val();

    if (go_to_contact_when_error == "True") {
        document.getElementById("contact_us").click(); // Click on the checkbox
    } 

    /*Cargamos todos los dias y los precios para introducir a posterior en el calendario*/
    $.ajax({ 
      //data: {'id_note': id_note}, 
      type: "GET", 
      url: "../get_apartment_prices_date",
      success: function(json) { 
        $.each(json.dates_prices, function(i, obj) {
          date_prices.push(obj.date);
          prices_down.push(obj.price_down);
          prices_up.push(obj.price_up);
        });
      },
      complete: function (json) {
        createCalendar()
        createListenersCalendar();
        calendar.refresh();
      },
      error : function(message) {
        console.log(message);
      }
    });

    $('#price-container').hide();
    //$('#book-now').hide();

    $("#StartDate").prop('disabled', true);
    $("#EndDate").prop('disabled', true);
    $("#StartDate" ).css({'background-color' : '#a19a9a'});
    $("#EndDate" ).css({'background-color' : '#a19a9a'});
    /*
    $("#StartDate" ).css({'background-color' : '#F5F5F5'});
    $("#EndDate" ).css({'background-color' : '#F5F5F5'});*/
  });

  function currentDate() {
    var d = new Date();
      var month = d.getMonth()+1;
      var day = d.getDate();
      return d.getFullYear() + '-' +
          (month<10 ? '0' : '') + month + '-' +
          (day<10 ? '0' : '') + day;
  }

  function createListenersCalendar () {
    calendar.onDateClick(function(event, date){
      let format_date_selected = jsCalendar.tools.dateToString(date, "yyyy-MM-DD");
      let isAlreadyChoise = days_rented_apartment.includes(format_date_selected);
      let current_date = currentDate();
      
      if (calendar.isSelected(date)) {
        calendar.unselect(date);
      } else {
        if (!isAlreadyChoise && format_date_selected > current_date) {
          //calendar.select(date);
          input.value = format_date_selected;
          if (input.id === "StartDate") {
            $( "#StartDate" ).trigger("change");
          } else {
            $( "#EndDate" ).trigger("change");
          }
        }
      }
    });

    calendar.onDateRender(function(date, element, info) {
      let radio_button_selected = $('input:radio[name=radio]:checked').attr('id')
      let format_date = jsCalendar.tools.dateToString(date, "yyyy-MM-DD");
      let current_date_without_format = new Date();
      current_date_without_format.setHours(0,0,0,0);
      let price = 0;
      let decimal_price = 0;

      element.style.fontSize = '11px';

      /*Array de dias con el precio*/
      if (date > current_date_without_format && date_prices.includes(format_date)) {
        let index = date_prices.indexOf(format_date);
        //let radio_button_selected = $('input:radio[name=optradio]:checked').attr('id')
        //element.style.fontWeight = 'bold';
        //element.style.color = '#c32525';
        if (radio_button_selected === "apartment-down") {
          price = prices_down[index].split('.')[0];
          decimal_price = prices_down[index].split('.')[1];
        } else if (radio_button_selected === "apartment-up") {
          price = prices_up[index].split('.')[0];
          decimal_price = prices_up[index].split('.')[1];
        } else {
          price = parseFloat(prices_up[index].split('.')[0]) + parseFloat(prices_down[index].split('.')[0]);
          decimal_price = price.toString().split('.')[1];
        }

        //var price = prices_down[index].split('.')[1];
        if(decimal_price==='00')
        {
          price=price.split('.')[0];
        }

        element.textContent += ' ' + price + '.-';

        /*Array de dias que ya estan ocupados*/
        if (date > current_date_without_format && (start_date_input_value == undefined || (date > start_date_input_value)) && days_rented_apartment.includes(format_date)) {
          element.style.color = '#c32525';
          element.textContent = date.getDate();
          element.style.textDecoration = "line-through";
        }
      }

      /*Ponemos en gris los dias anteriores al dia actual*/
      if (date < current_date_without_format || date < start_date_input_value) {
        element.style.color = '#bdbdbd';
      }
    });
  } 

  function createCalendar() {
    calendar = jsCalendar.new('#calendar_1');
    calendar.min("now");
    $('#calendar_1').addClass("material-theme yellow");
  }

  function fillDatesBetween (start_date, end_date){
    var dates_between = [];
    start_date = new Date(start_date);
    end_date = new Date(end_date);

    while (start_date <= end_date) {
      dates_between.push(start_date.getFullYear() + "-" + ("0" + (start_date.getMonth() + 1)).slice(-2) + "-" + ("0" + (start_date.getDate())).slice(-2));
      start_date.setDate(start_date.getDate() + 1);
    }

    return dates_between;
  }

  /*Validador para el telefono*/
  $(function () {
    $("#phone_id").keydown(function () {
      // Save old value.
      if (!$(this).val() || (parseInt($(this).val()) <= 99999999999 && parseInt($(this).val()) >= 0))
      $(this).data("old", $(this).val());
    });
    $("#phone_id").keyup(function () {
      // Check correct, else revert back to old value.
      if (!$(this).val() || (parseInt($(this).val()) <= 99999999999 && parseInt($(this).val()) >= 0))
        ;
      else
        $(this).val($(this).data("old"));
    });
  });

  /*Validador para el prefijo*/
    $(function () {
    $("#prefix_id").keydown(function () {
      // Save old value.
      if (!$(this).val() || (parseInt($(this).val()) <= 9999 && parseInt($(this).val()) >= 0))
      $(this).data("old", $(this).val());
    });
    $("#prefix_id").keyup(function () {
      // Check correct, else revert back to old value.
      if (!$(this).val() || (parseInt($(this).val()) <= 9999 && parseInt($(this).val()) >= 0))
        ;
      else
        $(this).val($(this).data("old"));
    });
  });

  // Collapse Navbar
  var navbarCollapse = function() {
    if ($("#mainNav").offset().top > 100) {
      $("#mainNav").addClass("navbar-shrink");
    } else {
      $("#mainNav").removeClass("navbar-shrink");
    }
  };
  // Collapse now if page is not at top
  navbarCollapse();
  // Collapse the navbar when page is scrolled
  $(window).scroll(navbarCollapse);

})(jQuery); // End of use strict
