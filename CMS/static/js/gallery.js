let modalId = $('#image-gallery');
let modalIdUp = $('#image-gallery-up');

$(document)
  .ready(function () {

    loadGallery(true, 'a.thumbnail');
    loadGalleryUp(true, 'a.thumbnail');

    //This function disables buttons when needed
    function disableButtons(counter_max, counter_current) {
      $('#show-previous-image, #show-next-image')
        .show();
      if (counter_max === counter_current) {
        $('#show-next-image')
          .hide();
      } else if (counter_current === 1) {
        $('#show-previous-image')
          .hide();
      }
    }

    function disableButtonsUp(counter_max, counter_current) {
      $('#show-previous-image-up, #show-next-image-up')
        .show();
      if (counter_max === counter_current) {
        $('#show-next-image-up')
          .hide();
      } else if (counter_current === 1) {
        $('#show-previous-image-up')
          .hide();
      }
    }

    /**
     *
     * @param setIDs        Sets IDs when DOM is loaded. If using a PHP counter, set to false.
     * @param setClickAttr  Sets the attribute for the click handler.
     */

    function loadGallery(setIDs, setClickAttr) {
      let current_image,
        selector,
        counter = 0;

      $('#show-next-image, #show-previous-image')
        .click(function () {
          if ($(this)
            .attr('id') === 'show-previous-image') {
            current_image--;
          } else {
            current_image++;
          }

          selector = $('[data-image-id="' + current_image + '"]');
          updateGallery(selector);
        });

      function updateGallery(selector) {
        let $sel = selector;
        current_image = $sel.data('image-id');
        $('#image-gallery-title')
          .text($sel.data('title'));
        $('#image-gallery-image')
          .attr('src', $sel.data('image'));
        disableButtons(counter, $sel.data('image-id'));
      }

      if (setIDs == true) {
        $('[data-image-id]')
          .each(function () {
            counter++;
            $(this)
              .attr('data-image-id', counter);
          });
      }

      $(setClickAttr)
        .on('click', function () {
          if ($(this).parent().parent().attr('id') === "gallery-container-down") 
            updateGallery($(this));
        });
    }

    function loadGalleryUp(setIDs, setClickAttr) {
      let current_image,
        selector,
        counter = 0;

      $('#show-next-image-up, #show-previous-image-up')
        .click(function () {
          if ($(this)
            .attr('id') === 'show-previous-image-up') {
            current_image--;
          } else {
            current_image++;
          }

          selector = $('[data-image-id-up="' + current_image + '"]');
          updateGalleryUp(selector);
        });

      function updateGalleryUp(selector) {
        let $sel = selector;
        current_image = $sel.data('image-id-up');
        $('#image-gallery-title-up')
          .text($sel.data('title'));
        $('#image-gallery-image-up')
          .attr('src', $sel.data('image'));
        disableButtonsUp(counter, $sel.data('image-id-up'));
      }

      if (setIDs == true) {
        $('[data-image-id-up]')
          .each(function () {
            counter++;
            $(this)
              .attr('data-image-id-up', counter);
          });
      }
      $(setClickAttr)
        .on('click', function () {
          if ($(this).parent().parent().attr('id') === "gallery-container-up") 
            updateGalleryUp($(this));           
        });
    }

  });

// build key actions
$(document)
  .keydown(function (e) {
    switch (e.which) {
      case 37: // left
        if ((modalId.data('bs.modal') || {})._isShown && $('#show-previous-image').is(":visible")) {
          $('#show-previous-image')
            .click();
        }
        if ((modalIdUp.data('bs.modal') || {})._isShown && $('#show-previous-image-up').is(":visible")) {
          $('#show-previous-image-up')
            .click();
        }
        break;

      case 39: // right
        if ((modalId.data('bs.modal') || {})._isShown && $('#show-next-image').is(":visible")) {
          $('#show-next-image')
            .click();
        }
        if ((modalIdUp.data('bs.modal') || {})._isShown && $('#show-next-image-up').is(":visible")) {
          $('#show-next-image-up')
            .click();
        }
        break;

      default:
        return; // exit this handler for other keys
    }
    e.preventDefault(); // prevent the default action (scroll / move caret)
  });
