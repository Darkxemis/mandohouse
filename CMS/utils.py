from django.core.mail import send_mail, EmailMessage
from .models import *
from decimal import Decimal

def send_email(from_user, to_user, subject, message, attach_file):
	message_obj = EmailMessage(subject, message, from_user, [to_user])
	if attach_file:
		message_obj.attach_file('media/main_img.png');

	message_obj.send()

def calculate_price(list_of_days, apartment_option, cleaning_switch):
	total_price = 0.00
	price = 0.00
	total_price = Decimal(total_price)
	price = Decimal(price)
	apartment = ""

	#Descuentos aplicables o dinero extra para cobrar
	clean_house_price = discount = DiscountAndExtraPrice.objects.filter(id=1).values('clean_house_price').first()['clean_house_price']
	discount = DiscountAndExtraPrice.objects.filter(id=1).values('discount').first()['discount']

	if apartment_option == "apartment-up":
		apartment = 'price_up'
	elif apartment_option == "apartment-down":
		apartment = 'price_down'

	for date in list_of_days:
		if apartment == "":
			price = Decimal(PricesPerDay.objects.filter(date=date).values('price_up').first()['price_up'])
			price += Decimal(PricesPerDay.objects.filter(date=date).values('price_down').first()['price_down'])
		else:
			price = Decimal(PricesPerDay.objects.filter(date=date).values(apartment).first()[apartment])

		total_price += price

	if get_boolean_cleaning_house(cleaning_switch):
		total_price += clean_house_price

	if len(list_of_days) >= 7:
		total_price -= (discount * total_price) / 100

	return str(total_price)

def get_image_and_description_apartment_up(language_code):
	if language_code == 'bg':
		return ApartmentUpImagen.objects.extra(
		    select={
		        'description_image': 'description_english',
		    }
		    ).values(
		    'description_image',
		    'image')
	elif language_code == 'da':
		return ApartmentUpImagen.objects.extra(
		    select={
		        'description_image': 'description_danish',
		    }
		    ).values(
		    'description_image',
		    'image')
	elif language_code == 'de':
		return ApartmentUpImagen.objects.extra(
		    select={
		        'description_image': 'description_deutch',
		    }
		    ).values(
		    'description_image',
		    'image')

def get_image_and_description_apartment_down(language_code):
	if language_code == 'bg':
		return ApartmentDownImagen.objects.extra(
		    select={
		        'description_image': 'description_english',
		    }
		    ).values(
		    'description_image',
		    'image')
	elif language_code == 'da':
		return ApartmentDownImagen.objects.extra(
		    select={
		        'description_image': 'description_danish',
		    }
		    ).values(
		    'description_image',
		    'image')
	elif language_code == 'de':
		return ApartmentDownImagen.objects.extra(
		    select={
		        'description_image': 'description_deutch',
		    }
		    ).values(
		    'description_image',
		    'image')

#Funcion que uso para el calculate price devuelvo true o false de las distita maneras que se le puede mandar
def get_boolean_cleaning_house(cleaning_switch):
	if cleaning_switch in (True, 'True', 'true', '1'):
		return True
	elif cleaning_switch in (False, 'False', 'false', '0'):
		return False

	return none

def get_language_code(code_language):
	if code_language == 'da':
		return 'da-dk'
	elif code_language == 'de':
		return 'de-at'
	elif code_language == 'bg':
		return 'en-us'



